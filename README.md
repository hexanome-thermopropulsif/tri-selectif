# Sommaire
* [Introdution](#introduction)
* [Remerciements](#remerciements)
* [Vidéo de présentation](#vidéo-de-presentation)
* [Slides de présentation](#slides)
* [Installation du jeu](#installation)
* [Notes de version](#notes-de-versions)
* [Unity Project Set-up](#project-setup)

# Introduction
Tri Sélect'IF est un jeu ayant pour but de sensibiliser les jeunes enfants de 
fin de maternelle et début de primaire (agés d'environ 5 à 8 ans) au tri des 
déchets.

Ce projet a été mené dans le cadre dans le cadre de la formation proposée par le
département Informatique de l'INSA de Lyon, ayant pour but de lier développement
logiciel et impact sociétal.
Tri Sélect'IF a été développé par l'hexanôme H4224 2019-2020.

Membres de l'hexanôme: 
- Matthieu Beaud
- Luc Billaud
- Quentin Ferro
- Félix Fonteneau
- Fabien Gelus
- Thomas Jorda


# Remerciements
Remerciements aux membres de l'équipe pédagogique, Lionel Brunie et Jean-Marc Petit, qui nous ont accompagnés durant ce projet, à P. Billaud pour les tests et retours, A. Billaud pour les dessins et M. Jorda pour la voix.

# Vidéo de présentation
La vidéo de présentation du jeu est disponible [ici](https://gitlab.com/MatthieuBeaud/pld-smart/-/blob/master/VidéoFinale.mp4) ou [ici (Youtube)](https://www.youtube.com/watch?v=TbIvNr034cg).

# Slides de la présentation de soutenance du projet
Les slides de la présentation de soutenance du projet sont disponibles [ici](https://gitlab.com/MatthieuBeaud/pld-smart/-/blob/master/Pr%C3%A9sentation-TriSelectIF.pptx).

# Installation
## Android 
 - Télécharger l'apk de la version 4.0 de l'application [ici](https://gitlab.com/MatthieuBeaud/pld-smart/-/blob/master/TriSelectIFv4.0.apk)
 - Mettre l'apk sur le smartphone sur lequel vous voulez installer le jeu
 - Dans les Paramètres de sécurité du smartphone, activer l'installation d'application à partir de sources inconnues
 - Exécuter l'apk depuis ce smartphone pour installer le jeu

**ou**

- Télécharger le projet ([voir Setup](#project-setup) pour les assets à importer)
- Build le projet pour Android avec Unity
- Réaliser les étapes ci-dessus avec l'apk obtenu

## iOS
(Suggestions, mais fonctionnement inconnu, nous avons tout fait sur Android)
 - Télécharger le projet sur un Mac ([voir Setup](#project-setup) pour les assets à importer)
 - Build le projet pour iOS avec Unity, l'application XCode est nécessaire (du moins sur macOS) pour ce build
 - Si build XCode, ouvrir le build dans XCode
 - Deploy sur le smartphone cible

# Notes de versions
Voici ci-dessous les notes de release de chaque version.
## 1.0
Première release voir branche release1.0

## 2.0
**Nouveautés**:
- ajout d'une musique de fond
- ajout d'un environnement pour combler le fond (qui ne sera pas forcement l'environnement final)
- ajout d'un bouton qui appelle un menu pause lors d'une partie
- ajout d'indicateurs pour indiquer la progression lors de la partie: les lancers restants, et le lancer en cours
- ajout d'effets sonores pour indiquer lorsqu'un lancé est réussi, ainsi que des particules de couleur verte
- ajout d'une mascotte qui est présente lors de tous menus et lors du jeu, quand on clique dessus, les conseils s'affichent (pour l'instant dans la console de debug, mais qui seront remplacés par des audios)
- ajout de 6 nouveaux déchets dans le jeu
- ajout de "Le saviez vous" dans le fenêtre de fin (affichage dans la console, qui seront remplacés par des audios avec une image)
(les nouveautés qui s'affichent dans la console ne sont donc malheureusement pas visibles sur mobile)

**Corrections**:
- changement de la couleur de la poubelle bleu en grise pour la poubelle générale
- augmentation de la vitesse d'un lancer de déchet vers la poubelle
- correction des règles de tri pour chaque déchet

## 3.0
**Nouveautés**:
- création et sauvegarde des profils d'utilisateur sur le smartphone
- création des boutons des différents profils dans la page d'accueil à partir des sauvegardes
- mise en place d'un menu "Paramètres" pour gérer les volumes des différentes actions (musiques, voix de mascotte, et effets sonores)
- mise en place de la page "En savoir plus" qui présente brièvement le projet
- enregistrement des paramètres des volumes de sons dans le profil (qui sont sauvegardés)
- ajout de statistiques dans le profil (qui sont sauvegardées également) comme le nombre de parties, le nombres de déchets bien triés dans chaque poubelle ...
- nouveau nom pour le jeu: Balance ton déchet --> Tri Sélect'IF
- icone spécifique pour l'application (sera surement remplacée par une icône de meilleure qualité)
- ajout de particules Oranges et Rouge pour un raté au premier essai, et un raté au 2ème essai
- refonte des scripts de Sound Management, avec ajout de sons sur les clics dans les menus, et continuité des effets sonores entre les différentes scène (plus d'interruption)
- refonte du menu pause, en développant une interface graphique au lieu de textuelle
- création d'un niveau de jeu facile, où, lors d'un premier échec, 2 poubelles disparaissent pour ne laisser le choix qu'entre 2
- ajout d'une mascotte unique qui accompagne le joueur, et qui réagit graphiquement à un bon ou un mauvais lancé
(les différents animaux ne servent que d'icone pour mieux identifier les profils mais ne sont pas la mascotte qui guide le joueur)
- réalisation des enregistrement d'une première version des fichiers audios, nécessaires pour faire parler la mascotte lors du jeu (en cours d'implémentation)

**Corrections**:
- changement des icônes proposées pour les profils (des icônes plus soignées)
- changement de la musique de fond (pour une musique libre de droits)
- amélioration de l'UI, notamment avec des nouveau graphiques, et un nouveau style
- changement de l'environnement de jeu, passant d'un triste mur en pierre à un sublime jardin
- faire en sorte que les animations des poubelles et des particules se finissent avant de pouvoir lancer un nouveau déchet
- correction de bugs divers

## 4.0
**Nouveautés**:
- texte de l'écran de fin de partie qui change selon le résultat
- nouvelle icône d'application
- ajout d'une limite de 6 au nombre maximum de profil créés
- ajout de la possibilité de supprimer un profil
- ajout d'un menu Profil pour accéder à ses stats de jeu accessible depuis un bouton à l'image de l'icône du profil
- ajout d'un menu Point Culture qui regroupe tous les points cultures du jeu et qui peuvent être écoutés
- image et nom par défaut lors de la création d'un profil sans choisir d'image ni entrer de nom
- la mascotte peut maintenant parler lorsqu'on lui clique dessus
- réglages des volumes accessibles depuis le menu pause
- implémentation d'un niveau difficile avec une limite de temps
- stockage du meilleur temps du niveau difficile dans le profil
- ajout d'une pop up lorsqu'on essaie de créer plus de 6 profils
- ajout d'écran de chargement qui display un point culture au hasard, avant et après une partie
- mise en place d'un tutoriel guidé par la voix de la mascotte lorsqu'on crée un nouveau profil

**Corrections**:
- normalisation, et nettoyage des fichiers audio de la Mascotte
- augmentation du gain pour l'audio d'un échec
- mise en page des boutons profils en fonction du nombre
- clean et resize (pour compression) des icônes de profils (animaux)
- amélioration de la qualité des icônes poubelles
- correction de bugs divers

# Project Setup
Le projet a été réalisé sur Unity version 2019.3.9f1
## Unity Assets utilisé
Voici la liste des assets du Unity Asset Store qui sont nécessaires pour ce
projet:
 - https://assetstore.unity.com/packages/2d/textures-materials/floors/graveltexture4k-71508
 - https://assetstore.unity.com/packages/2d/textures-materials/grass-flowers-pack-free-138810
 - https://assetstore.unity.com/packages/2d/textures-materials/grass-and-flowers-pack-1-17100
 - https://assetstore.unity.com/packages/3d/cola-can-96659
 - https://assetstore.unity.com/packages/3d/props/cardboard-boxes-pack-30695
 - https://assetstore.unity.com/packages/3d/props/exterior/plastic-trash-bins-160771

(vérifier s'ils sont importés automatiquement, ou non)

