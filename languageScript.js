var rad = document.getElementsByName('language');
for (var i = 0; i < rad.length; i++) {
    rad[i].addEventListener('change', function() {
        console.log(this.value)
        if(this.value =="fr"){
            setLangFr();
        }
        if(this.value=="en"){
            setLangEn();
        }
    });
}

function setLangEn() {
    var fr = document.getElementsByClassName("fr");
    var en = document.getElementsByClassName("en");

    for (let f = 0; f < fr.length; f++) {
        const element = fr[f];
        element.style.display = "none";
    }

    for (let e = 0; e < en.length; e++) {
        const element = en[e];
        element.style.display = "inline";
    }
}

function setLangFr() {
    var fr = document.getElementsByClassName("fr");
    var en = document.getElementsByClassName("en");

    for (let e = 0; e < en.length; e++) {
        const element = en[e];
        element.style.display = "none";
    }

    for (let f = 0; f < fr.length; f++) {
        const element = fr[f];
        element.style.display = "inline";
    }
}