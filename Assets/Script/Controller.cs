﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class Controller : MonoBehaviour
{
    public static int SERIES_COUNT = 10;    //nb de déchets à lancer dans une serie
    public static float TIME_TIMIT = 80f;   //limite du temps autorise en mode difficile
    float timeSpent = 0f;
    bool isTimeTicking = true;


    [SerializeField]
    private bool gameRunning = true;
    private static int niveau;


    public GameObject bin1;
    public GameObject bin2;
    public GameObject bin3;
    public GameObject bin4;

    public Trash[] trashList;
    private List<Trash> trashToThrow = new List<Trash>();
    public Transform respawnPosition;
    private Trash currentTrashType;
    private GameObject currentTrashObject;

    public AudioClip successSound;
    public AudioClip failSound;

    public Texture2D mascotteContente;
    public Texture2D mascotteNormale;
    public Texture2D mascotteTriste;

    private RawImage imageMascotte;

    ScoreManager sm;

    bool secondTry = false;

    int trashCount = 0;

    // Start is called before the first frame update
    void Start()
    {
        sm = ScoreManager.Instance;
        sm.init();

        isTimeTicking = true;
        GameObject progressBar = GameObject.Find("ProgressBar");
        if (niveau == 3)
        {
            progressBar.SetActive(true);
        }
        if (niveau != 3)
        {
            progressBar.SetActive(false);
        }

        imageMascotte = GameObject.Find("Mascotte Button").GetComponentInChildren<RawImage>();


        MascotteManager.resetClickCount();

        if (niveau != 0)
        {
            initTrashToThrow();
        }
        else
        {
            initTutorialTrashToThrow();
        }

        if (gameRunning)
        {
            spawnNewTrash();
        }


    }
    void Update()
    {
        if (niveau == 3)
        {
            if (isTimeTicking && timeSpent < TIME_TIMIT)
            {
                timeSpent += Time.deltaTime;
                updateProgressBar();
            }
            else
            {
                isTimeTicking = false;
                timeSpent = TIME_TIMIT;
                updateProgressBar();
                StartCoroutine(waitAndEndGame());
            }
        }
    }

    private void initTrashToThrow()
    {
        foreach(var t in trashList)
        {
            trashToThrow.Add(t);
        }
    }

    private void initTutorialTrashToThrow()
    {
        foreach(var t in trashList)
        {
            if(t.nom.Equals("une boite en carton") || t.nom.Equals("une bouteille en verre") || t.nom.Equals("du pain") || t.nom.Equals("un toaster"))
            {
                trashToThrow.Add(t);
            }
        }
    }

    public void lancer(GameObject poubelle) {
        //Pour l'instant on respawn un nouvel objet
        if (testTrashAndBin(currentTrashType, poubelle)) {
            bonLancer(poubelle);
        } else if (secondTry) {
            mauvaisLancer2(poubelle);
        } else {
            mauvaisLancer1(poubelle);
        }
    }

    private void bonLancer(GameObject poubelle) {
        imageMascotte.texture = mascotteContente;
        sendSuccessParticles(poubelle);
        EffectsSoundsManager.playSuccessSound();
        MascotteManager.bravo();
        MascotteManager.resetClickCount();
        sm.setResult(trashCount, currentTrashType, poubelle.name, true);

        if(niveau == 0)
        {
            MascotteManager.tutorialInfoPoubelle(poubelle.name);

            if (poubelle.name.Equals("TrashbinRed"))
            {
                StartCoroutine(waitForEndOfTutorial(MascotteManager.getVoice()));
            }
        }

        StartCoroutine(ReactivateBinsAndSpawnNewTrash());
        secondTry = false;
    }

    private void mauvaisLancer1(GameObject poubelle) {
        imageMascotte.texture = mascotteTriste;

        sendTryAgainParticles(poubelle);
        EffectsSoundsManager.playFailSound();
        if(niveau != 3)
        {
            MascotteManager.tryAgain();
        }
        
        StartCoroutine(doBadThrowActions(MascotteManager.getVoice(), poubelle));
        /*if (niveau == 1) {
            GameObject deletedBin = chooseWrongBin(poubelle);
            StartCoroutine(DisactivateBinsAndSpawnTrash(deletedBin, poubelle));
        }
        else {
            StartCoroutine(WaitAndSpawnTrash());
        }

        secondTry = true;*/
    }

    IEnumerator doBadThrowActions(AudioSource source, GameObject poubelle)
    {
        if(niveau != 3)
        {
            if (source.isPlaying)
            {
                yield return new WaitForSeconds(source.clip.length - source.time);
            }
            else
            {
                yield return null;
            }

            if (niveau == 1)
            {

                MascotteManager.tryAgain2();
            }
        }
        yield return new WaitForSeconds(0.5f);

        if (niveau == 1)
        {
            GameObject deletedBin = chooseWrongBin(poubelle);
            StartCoroutine(DisactivateBinsAndSpawnTrash(deletedBin, poubelle));
        }
        else
        {
            StartCoroutine(WaitAndSpawnTrash());
        }

        if (source.isPlaying)
        {
            yield return new WaitForSeconds(source.clip.length - source.time);
        }
        else
        {
            yield return null;
        }
        MascotteManager.tryAgain3(currentTrashType);

        secondTry = true;

    }

    private void mauvaisLancer2(GameObject poubelle) {
        imageMascotte.texture = mascotteTriste;
        sendFailParticles(poubelle);
        EffectsSoundsManager.playFailSound();
        MascotteManager.fail(currentTrashType);
        MascotteManager.resetClickCount();
        sm.setResult(trashCount, currentTrashType, poubelle.name, false);
        StartCoroutine(ReactivateBinsAndSpawnNewTrash());
        secondTry = false;
    }

    private GameObject chooseWrongBin(GameObject poubelle) {
        GameObject bin = bin1;

        do
        {
            int rand = Random.Range(1, 5);

            switch (rand)
            {
                case 1:
                    bin = bin1;
                    break;
                case 2:
                    bin = bin2;
                    break;
                case 3:
                    bin = bin3;
                    break;
                case 4:
                    bin = bin4;
                    break;
                default:
                    break;
            }
        } while (bin == poubelle || testTrashAndBin(currentTrashType, bin));

        return bin;
    }

    IEnumerator DisactivateBinsAndSpawnTrash(GameObject deletedBin, GameObject poubelle) {
        //desactiver clic pour ne pas pouvoir cliquer lorsqu'un dechet n'est pas spawn
        DesactiverClic();

        float duration = poubelle.GetComponent<ParticleSystem>().main.duration; //Durée de l'animation des particules
        yield return new WaitForSeconds(1f); //Ca marche bien mais on pourrait remplacer par la valeur dynamique "duration"
        deletedBin.SetActive(false);
        poubelle.SetActive(false);

        //reactiver clic
        ReactiverClic();

        spawnTrash(currentTrashType);
        imageMascotte.texture = mascotteNormale;
    }

    IEnumerator ReactivateBinsAndSpawnNewTrash() {
        //desactiver clic
        DesactiverClic();

        float duration = bin1.GetComponent<ParticleSystem>().main.duration; //Durée de l'animation des particules
        yield return new WaitForSeconds(1f); //Ca marche bien mais on pourrait remplacer par la valeur dynamique "duration"
        bin1.SetActive(true);
        bin2.SetActive(true);
        bin3.SetActive(true);
        bin4.SetActive(true);

        //reactiver clic
        ReactiverClic();

        spawnNewTrash();
        imageMascotte.texture = mascotteNormale;
    }

    IEnumerator WaitAndSpawnTrash() {
        //desactiver clic
        DesactiverClic();

        float duration = bin1.GetComponent<ParticleSystem>().main.duration;
        yield return new WaitForSeconds(duration - 0.2f);

        //reactiver clic
        ReactiverClic();

        spawnTrash(currentTrashType);
        imageMascotte.texture = mascotteNormale;
    }

    private void OnlyActivateGreenBin()
    {
        bin2.SetActive(false);
        bin3.SetActive(false);
        bin4.SetActive(false);
    }

    private void OnlyActivateRedBin()
    {
        bin1.SetActive(false);
        bin3.SetActive(false);
        bin4.SetActive(false);
    }

    private void OnlyActivateYellowBin()
    {
        bin2.SetActive(false);
        bin1.SetActive(false);
        bin4.SetActive(false);
    }

    private void OnlyActivateGreyBin()
    {
        bin2.SetActive(false);
        bin3.SetActive(false);
        bin1.SetActive(false);
    }

    private void DesactiverClic() {
        bin1.layer = 2;
        bin2.layer = 2;
        bin3.layer = 2;
        bin4.layer = 2;
    }

    private void ReactiverClic() {
        bin1.layer = 0;
        bin2.layer = 0;
        bin3.layer = 0;
        bin4.layer = 0;
    }

    private void endSet()
    {
        isTimeTicking = false;
        ScoreManager.Instance.time = timeSpent;
        SceneManager.LoadScene("EndGameScene");
    }

    private void spawnNewTrash()
    {
        trashCount++;
        if (trashCount > SERIES_COUNT) {

            endSet();
        }
        else {
            if(trashToThrow.Count == 0 && niveau != 0)
            {
                initTrashToThrow();
            }
            else if (trashToThrow.Count == 0 && niveau == 0)
            {
                if (trashToThrow.Count == 0 && niveau == 0)
                {
                    //MascotteManager.endTutorialSpeech();
                    //TutorialManager.endGameplayTutorial();
                }
            }
            
            int randNum = 0;
            if(niveau != 0)
            {
                randNum = Random.Range(0, trashToThrow.Count);
            }

            if (trashToThrow.Count > 0)
            {
                spawnTrash(trashToThrow[randNum]);
                if (niveau == 0)
                {
                    if (testTrashAndBin(trashToThrow[randNum], bin1))
                    {
                        StartCoroutine(waitForEndOfAudioAndPlay(MascotteManager.getVoice(), "Verte"));
                        //MascotteManager.tutorialPoubelle("Verte");
                        OnlyActivateGreenBin();
                    }
                    else
                    {
                        if (testTrashAndBin(trashToThrow[randNum], bin2))
                        {
                            StartCoroutine(waitForEndOfAudioAndPlay(MascotteManager.getVoice(), "Rouge"));
                            //MascotteManager.tutorialPoubelle("Rouge");
                            OnlyActivateRedBin();
                        }
                        else
                        {
                            if (testTrashAndBin(trashToThrow[randNum], bin3))
                            {
                                StartCoroutine(waitForEndOfAudioAndPlay(MascotteManager.getVoice(), "Jaune"));
                                //MascotteManager.tutorialPoubelle("Jaune");
                                OnlyActivateYellowBin();
                            }
                            else
                            {
                                if (testTrashAndBin(trashToThrow[randNum], bin4))
                                {
                                    StartCoroutine(waitForEndOfAudioAndPlay(MascotteManager.getVoice(), "Grise"));
                                    //MascotteManager.tutorialPoubelle("Grise");
                                    OnlyActivateGreyBin();
                                }
                            }
                        }
                    }
                }
                trashToThrow.RemoveAt(randNum);

            }
        }
    }
    
    IEnumerator waitForEndOfTutorial(AudioSource source)
    {
        if (source.isPlaying)
        {
            yield return new WaitForSeconds(source.clip.length - source.time);
        }
        else
        {
            yield return null;
        }
        MascotteManager.endTutorialSpeech();

        yield return new WaitForSeconds(1.0f);

        if (source.isPlaying)
        {
            yield return new WaitForSeconds(source.clip.length - source.time);
        }
        else
        {
            yield return null;
        }

        TutorialManager.endGameplayTutorial();

    }
    IEnumerator waitForEndOfAudioAndPlay(AudioSource audiosource, string color)
    {
        if (audiosource.isPlaying)
        {
            yield return null;
            DesactiverClic();
            yield return new WaitForSeconds(audiosource.clip.length - audiosource.time);
            ReactiverClic();
            yield return null;

        }
        else
        {
            yield return null;
        }
        MascotteManager.tutorialPoubelle(color);
    }

    private void spawnTrash(Trash trashtype) {
        currentTrashType = Instantiate(trashtype);
        currentTrashObject = Instantiate(currentTrashType.model);
        currentTrashObject.transform.position = respawnPosition.position + currentTrashType.relativePosition;
        currentTrashObject.transform.localScale = currentTrashType.localScale;
        currentTrashObject.transform.eulerAngles = currentTrashType.rotation;
        currentTrashObject.AddComponent<TrashThrow>().controller = this;
        currentTrashObject.layer = 2; //pour ignore les raycast
    }

    private bool testTrashAndBin(Trash trash, GameObject bin) {
        return bin.GetComponent<BinAttributes>().listDechets.Contains(trash);
    }

    private void sendSuccessParticles(GameObject bin)
    {
        var ps = bin.GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = new Color(0, 240, 0);
        ps.Play();
    }

    private void sendFailParticles(GameObject bin)
    {
        var ps = bin.GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = new Color(240, 0, 0);
        ps.Play();
    }

    private void sendTryAgainParticles(GameObject bin)
    {
        var ps = bin.GetComponent<ParticleSystem>();
        var main = ps.main;
        main.startColor = new Color(240, 150, 0);
        ps.Play();
    }

    public void onMascotteClick()
    {
        MascotteManager.mascotteClickInGame(currentTrashType);
    }

    public static void SetNiveau(int level) {
        niveau = level;
    }

    public static int GetNiveau()
    {
        return niveau;
    }


    private void updateProgressBar()
    {
        float progress = timeSpent / TIME_TIMIT;
        float width = GameObject.Find("ProgressBar").GetComponent<RectTransform>().rect.width;
        GameObject.Find("Fill").GetComponent<RectTransform>().sizeDelta = new Vector2(width * progress, 0);
    }

    IEnumerator waitAndEndGame()
    {
        DesactiverClic();
        //jouer une musique de fin de partie ou afficher un truc ?
        yield return new WaitForSeconds(5);
        endSet();
    }
}
