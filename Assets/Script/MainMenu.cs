﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class MainMenu : MonoBehaviour {

     public void MenuScene ()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        if (SceneManager.GetActiveScene().name == ("ParamScene"))  //on est dans les params et on retourne au menu principal, on save les chgts de vol
        {
            VolumeManager.saveVolsChanges();
        }
        if (PlayerProfileManager.usedProfile.mainMenuTutorial)
        {
            if (PlayerProfileManager.usedProfile.presentationTutorial)
            {
                TutorialManager.endPresentationTutorial();
            }
            SceneManager.LoadScene("TutorialMenuScene");
            TutorialManager.startMainMenuTutorial();
        }
        else
        {
            SceneManager.LoadScene("MenuScene");
        }
    }
    

     public void ChooseLevel () 
     {
          EffectsSoundsManager.playClicSound();
          
        if (PlayerProfileManager.usedProfile.levelSelectionTutorial)
        {
            SceneManager.LoadScene("TutorialChoixNiveauScene");
            TutorialManager.startDifficultyTutorial();
        }
        else
        {
            SceneManager.LoadScene("ChoixNiveauScene");
        }
    }

    public void LoadScene(string nextScene)
    {
        SceneManager.LoadScene("LoadingScene");
        LoadingSceneScript.nextScene = nextScene;
    }

     public void PlayGameEasy ()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene ("LoadingScene");
        LoadingSceneScript.nextScene = "GameScene";
        Controller.SetNiveau(1);
        MascotteManager.letsGo();
     }

     public void PlayGameNormal ()
     {
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene ("LoadingScene");
        LoadingSceneScript.nextScene = "GameScene";
        Controller.SetNiveau(2);
        MascotteManager.letsGo();
     }

    public void PlayGameHard ()
    {
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene ("LoadingScene");
        LoadingSceneScript.nextScene = "GameScene";
        Controller.SetNiveau(3);
        MascotteManager.letsGo();
    }
    public void PlayTutorial()
    {
        EffectsSoundsManager.playClicSound();
        Controller.SetNiveau(0);
        LoadingSceneScript.nextScene = "GameScene";
        SceneManager.LoadScene("LoadingScene");
        MascotteManager.letsGo();
    }

    public static void EndTutorial()
    {
        Controller.SetNiveau(1);
        LoadingSceneScript.nextScene = "GameScene";
        SceneManager.LoadScene("LoadingScene");
        MascotteManager.letsGo();
    }

    public void CreateProfile()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene("CreateProfileScene");
     }

     public void ChooseProfile()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene("ChooseProfileScene");
     }

     public void GoToParams()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene("ParamScene");
     }

     public void GoToEnSavoirPlus()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene("EnSavoirPlusScene");
     }


     public void QuitGame ()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        Application.Quit();
     }

     public void GoToProfile()
     {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene("ProfilScene");
     }

    public void GoToFunFacts()
    {
        MascotteManager.stopAudio();
        EffectsSoundsManager.playClicSound();
        SceneManager.LoadScene("FunFactsScene");
    }

}
