﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class ProfileButtonSpawner : MonoBehaviour
{
   

    public GameObject buttonCanvas;
    public GameObject baseButton;

    private List<Profile> savedProfiles;
    private int nbProfiles;

    // Start is called before the first frame update
    void Start()
    {
        loadSavedProfiles();
        displayProfileButtons();
        if ( !(PlayerProfileManager.usedProfile is null) && PlayerProfileManager.usedProfile.presentationTutorial)
        {
            TutorialManager.startPresentationTutorial();
        }
    }

    private void displayProfileButtons()
    {
        //spawnButton();
        foreach (Profile profile in savedProfiles)
        {
            spawnButton(profile);
        }
    }

    private void loadSavedProfiles()
    {
        savedProfiles = new List<Profile>();
        nbProfiles = 1;

        string playerPrefKey = "profile" + nbProfiles;

        while (nbProfiles<=6)
        {
            if (PlayerPrefs.HasKey(playerPrefKey)) {
                Profile profile = ScriptableObject.CreateInstance<Profile>();
                JsonUtility.FromJsonOverwrite(PlayerPrefs.GetString(playerPrefKey), profile);
                savedProfiles.Add(profile);
            }
            
            nbProfiles++;
            playerPrefKey = "profile" + nbProfiles;
        }
    }

    private GameObject spawnButton(Profile profile)
    {
        var button = Instantiate(baseButton) as GameObject;
        button.name = profile.playerPrefKey;

        button.GetComponent<RectTransform>().SetParent(buttonCanvas.GetComponent<RectTransform>(), false);

        RectTransform parent = buttonCanvas.GetComponent<RectTransform>();

        GridLayoutGroup grid = buttonCanvas.GetComponent<GridLayoutGroup>();
        //gestion de la position si il y a 1, ou 2 lignes d'icones profil
        if (savedProfiles.Count > 3)
        {
            grid.childAlignment = TextAnchor.UpperCenter;
        }
        else
        {
            grid.childAlignment = TextAnchor.MiddleCenter;
        }

        button.GetComponentInChildren<TextMeshProUGUI>().SetText(profile.profileName);

        Texture image = Resources.Load<Texture2D>(profile.imagePath);
        button.GetComponentInChildren<RawImage>().texture = image;

        button.GetComponent<Button>().onClick.AddListener(() => onButtonClick(profile));

        return button;
    }

    private void onButtonClick(Profile chosenProfile)
    {
        PlayerProfileManager.usedProfile = chosenProfile;
        VolumeManager.setVolsPlayer();  //on charge les volumes du profil actuel;
        EffectsSoundsManager.playClicSound();
        MainMenu script = FindObjectOfType<MainMenu>();
        script.MenuScene(); //Menu scene stops Presentation Tutorial if it was
    }
}
