﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.UI;
using UnityEngine.SceneManagement;

public class TutorialManager : MonoBehaviour
{
    public static Controller controller;

    public static void startPresentationTutorial()
    {
        // Desactiver les boutons de la scène
        GameObject objet = GameObject.Find("Profiles GameObject");
        Component[] buttonList = objet.GetComponentsInChildren<Button>();
        foreach (Button button in buttonList)
        {
            if (!button.name.Equals(PlayerProfileManager.usedProfile.playerPrefKey))
            {
                button.interactable = false;
            }
        }
        string animal = PlayerProfileManager.usedProfile.imagePath;
        if (animal.Equals("Icones/elephant"))
        {
            MascotteManager.tutorialConnexion("Elephant");
        }
        else
        {
            if (animal.Equals("Icones/lion"))
            {
                MascotteManager.tutorialConnexion("Lion");
            }
            else
            {
                if (animal.Equals("Icones/oiseau"))
                {
                    MascotteManager.tutorialConnexion("Oiseau");
                }
                else
                {
                    MascotteManager.tutorialConnexion("RatonLaveur");
                }
            }
        }
        GameObject.Find("CreerProfilButton").GetComponent<Button>().interactable = false;
        // La mascotte se présente
        // Creer un bouton "ok" pour enclencher la prochaine phrase de la mascotte et être sur que l'enfant suit (ne pas pouvoir cliquer sur ce bouton avant la fin de la phrase la mascotte)
        // Utiliser le bouton entre chaque phrase du tutoriel ( et permettre de répeter la phrase en cliquant sur la mascotte )
        // Expliquer le fonctionnement de la page de choix de profil (essayer de se rappeler de l'animal qu'il a choisit pour lui indiquer quel est son profil ? )
        // Activer uniquement le bouton permettant de se connecter au profil qui vient d'être créé et lui demander d'appuyer
        // desactiver le tutoriel pour le compte en cours
    }

    public static void endPresentationTutorial()
    {
        PlayerProfileManager.updatePresentationTutorial();
    }



    public static void startMainMenuTutorial()
    {
        /* SceneManager.LoadScene("TutorialMenuScene");
        // Desactiver tous les boutons de la scene
        // Présentation du menu (même format que pour la page d'avant)
        // activer le bouton pour jouer et le faire appuyer
        // désactiver le tutoriel pour le compte en cours
        print("Buttons desactivated");*/
        MascotteManager.tutorialMainMenu();
        PlayerProfileManager.updateMenuTutorial();
    }

    public static void startDifficultyTutorial()
    {
        MascotteManager.tutorialDifficultyLevel();
    }

    public static void endGameplayTutorial()
    {
        PlayerProfileManager.updateGameplayTutorial();
        PlayerProfileManager.updateDifficultyTutorial();
        MainMenu.EndTutorial();
    }
}
