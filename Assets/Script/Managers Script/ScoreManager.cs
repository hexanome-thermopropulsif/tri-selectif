﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreManager : MonoBehaviour
{
    int[] results = new int[Controller.SERIES_COUNT];
    Trash[] trashes = new Trash[Controller.SERIES_COUNT];
    string[] bins = new string[Controller.SERIES_COUNT];

    public TextMeshProUGUI scoreCounter_TMP;

    private int score;
    public float time;

    public Texture2D iconeDechet;
    public int iconSize = 30;
    public Texture2D arrowDesign;
    public int arrowSize = 10;

    private float scale;

    Color neutralColor = new Color(0f,0f,0f);
    Color checkColor = new Color(0f,0.75f,0.25f);
    Color failColor = new Color(0.75f,0f,0.25f);

    GameObject[] myObject = new GameObject[Controller.SERIES_COUNT];
    RectTransform[] rt = new RectTransform[Controller.SERIES_COUNT];
        
    GameObject arrow;
    RectTransform arrowRT;

    private static ScoreManager instance = null;
    public static ScoreManager Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
        //DontDestroyOnLoad(this.gameObject);
    }

    public void init() {
        GameObject mPanel = GameObject.Find("ScorePanel");

        GameObject can = GameObject.Find("Canvas");
        scale = can.GetComponent<Canvas>().scaleFactor;
         
        for (int i = 0; i < results.Length; i++)
        {
            myObject[i] = new GameObject();
            myObject[i].AddComponent<RawImage>().texture = iconeDechet;
            myObject[i].GetComponent<RawImage>().color = neutralColor;

            rt[i] = myObject[i].GetComponent<RectTransform>();
            rt[i].SetParent(mPanel.transform);

            rt[i].anchorMin = new Vector2(1f, 1f);
            rt[i].anchorMax = new Vector2(1f, 1f);
            rt[i].pivot = new Vector2(0.5f, 0.5f);
            rt[i].anchoredPosition = new Vector3(-(float)(5+iconSize*(myObject.Length-i-1)+iconSize/2), -(float)(5+iconSize/2), 0);
            rt[i].sizeDelta = new Vector2(scale*(float)iconSize, scale*(float)iconSize);
        }        

        arrow = new GameObject();
        arrow.AddComponent<RawImage>().texture = arrowDesign;

        arrowRT = arrow.GetComponent<RectTransform>();
        arrowRT.SetParent(mPanel.transform);
        arrowRT.anchorMin = new Vector2(1f, 0f);
        arrowRT.anchorMax = new Vector2(1f, 0f);
        arrowRT.pivot = new Vector2(0.5f, 0.5f);
        arrowRT.sizeDelta = new Vector2(scale*(float)arrowSize, scale*(float)arrowSize);

        highlightLancerEnCours(0);
    }

    public void setResult(int numeroDeLancer, Trash trash, string poubelleName, bool resultat)
    {
        if (numeroDeLancer >= 1 && numeroDeLancer <= results.Length) {
            //numeroDeLancer - 1 car les tableaux commencent à 0.
            trashes[numeroDeLancer-1] = trash;
            bins[numeroDeLancer-1] = poubelleName;
            results[numeroDeLancer-1] = resultat ? 1 : 2;

            if (resultat) score++;

            updateAffichage(numeroDeLancer-1);
        }
        else {
            Debug.LogWarning("Numéro de lancer incohérent !");
        }
    }

    private void updateAffichage(int numeroDeLancer) {
        scoreCounter_TMP.text = score.ToString();

        if (results[numeroDeLancer] == 1) {
           myObject[numeroDeLancer].GetComponent<RawImage>().color = checkColor;
        }
        else if (results[numeroDeLancer] == 2) {
           myObject[numeroDeLancer].GetComponent<RawImage>().color = failColor;
        }
        else {
           myObject[numeroDeLancer].GetComponent<RawImage>().color = neutralColor;
        }
        highlightLancerEnCours(numeroDeLancer+1);
    }

    private void highlightLancerEnCours(int numeroDeLancer) {
        arrowRT.anchoredPosition = new Vector3(-(float)(5+iconSize*(myObject.Length-numeroDeLancer-1)+iconSize/2.0f), (float)(5+arrowSize/2), 0);
    }

    public int getResult(int lancer) {
        return results[lancer];
    }

    public string getBinName(int lancer) {
        return bins[lancer];
    }

    public Trash getTrash(int lancer) {
        return trashes[lancer];
    }

    public int getScore() {
        return score;
    }
}
