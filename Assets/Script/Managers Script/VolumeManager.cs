﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VolumeManager : MonoBehaviour
{
    //le game object de ce script peut passer de scene en scene sans etre detruit
    //et il ne peut y en avoir que 1 par scene sinon ca le destroy

    public static float musicVolume = 0.1f;
    public static float mascotteVolume = 0.5f;
    public static float effectsVolume = 0.25f;



    private static VolumeManager instance = null;
    public static VolumeManager Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
            GameObject.Find("BackGroundSound").GetComponent<AudioSource>().volume = musicVolume;
            GameObject.Find("EffectsSounds").GetComponent<AudioSource>().volume = effectsVolume;
            GameObject.Find("MascotteSounds").GetComponent<AudioSource>().volume = mascotteVolume;
        }
        DontDestroyOnLoad(this.gameObject);
    }

    public static void setVolsPlayer()
    {
        musicVolume = PlayerProfileManager.usedProfile.musicVol;
        mascotteVolume = PlayerProfileManager.usedProfile.mascotteVol;
        effectsVolume = PlayerProfileManager.usedProfile.effectsVol;
        GameObject.Find("BackGroundSound").GetComponent<AudioSource>().volume = musicVolume;
        GameObject.Find("EffectsSounds").GetComponent<AudioSource>().volume = effectsVolume;
        GameObject.Find("MascotteSounds").GetComponent<AudioSource>().volume = mascotteVolume;
    }

    public static void saveVolsChanges()
    {
        PlayerProfileManager.updateSoundParameters(musicVolume, mascotteVolume, effectsVolume);
    }

    public static void setMusicVolume(float volume) {
        musicVolume = volume;
        GameObject.Find("BackGroundSound").GetComponent<AudioSource>().volume = musicVolume;
    }

    public static void setMascotteVolume(float volume) {
        mascotteVolume = volume;
        GameObject.Find("MascotteSounds").GetComponent<AudioSource>().volume = mascotteVolume;
    }

    public static void setEffectsVolume(float volume) {
        effectsVolume = volume;
        GameObject.Find("EffectsSounds").GetComponent<AudioSource>().volume = effectsVolume;
    }
}
