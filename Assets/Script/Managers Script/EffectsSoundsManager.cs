﻿using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.XR;

public class EffectsSoundsManager : MonoBehaviour
{

    public static AudioClip clicSound;
    public static AudioClip successSound;
    public static AudioClip failSound;

    static AudioSource audioSource;

    // Start is called before the first frame update
    void Start()
    {
        clicSound = Resources.Load("Sounds/clicSoft") as AudioClip;
        successSound = Resources.Load("Sounds/correctSound3") as AudioClip;
        failSound = Resources.Load("Sounds/falseAnswer1") as AudioClip;

        audioSource = this.GetComponent<AudioSource>();
    }

    public static void playSuccessSound()
    {
        audioSource.PlayOneShot(successSound);
    }

    public static void playFailSound()
    {
        audioSource.PlayOneShot(failSound);
    }

    public static void playClicSound()
    {
        audioSource.PlayOneShot(clicSound);
    }
}
