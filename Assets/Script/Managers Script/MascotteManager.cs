﻿using System;
using UnityEngine;

public class MascotteManager : MonoBehaviour
{
    private static int clickCounter = 0;
    private static AudioClip mascotteVoice;
    private static AudioSource voice;   //c'est l'audio source du MascotteSoundsMananger
    private static System.Random random;

    void Start()
    {
        voice = GameObject.Find("MascotteSounds").GetComponent<AudioSource>(); //c'est l'audio source du MascotteSoundsMananger
        random = new System.Random();
    }

    public static void playClip(AudioClip clip) {
        stopAudio();
        voice.clip = clip;
        voice.Play();
    }

    public void mascotteClickMenu ()
    {
        aideMenu(clickCounter);
        clickCounter++;
        if(clickCounter > 4)
        {
            clickCounter = 0;
        }
    }

    public void mascotteClickSelectNiveau()
    {
        switch (clickCounter)
        {
            case 0:
                aideChoixNiveau();
                clickCounter++;
                break;
            case 1:
                aideMenuNiveau();
                clickCounter = 0;
                break;
        }
    }

    public static void mascotteClickInGame(Trash trash)
    {
        switch (clickCounter)
        {
            case 0:
                presentationDechet(trash);
                clickCounter++;
                break;
            case 1:
                presentationPoubelles("grise");
                clickCounter++;
                break;
            case 2:
                presentationPoubelles("jaune");
                clickCounter++;
                break;
            case 3:
                presentationPoubelles("verte");
                clickCounter++;
                break;
            case 4:
                presentationPoubelles("rouge");
                clickCounter++;
                break;
            case 5:
                encouragement();
                clickCounter = 0;
                break;
        }
    }

    public static void resetClickCount()
    {
        clickCounter = 0;
    }

    private static void encouragement()
    {
        int clip = random.Next(1, 5);
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Motivation/" + clip);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void presentationPoubelles(string couleur)
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Poubelle/"+couleur);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void presentationDechet(Trash dechet)
    {
        mascotteVoice = dechet.presentationClip;
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void letsGo()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/LetsGo");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void bravo()
    {
        int clip = random.Next(1, 14);
        mascotteVoice = (AudioClip) Resources.Load("MascotteSpeech/Success/"+clip);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tryAgain()
    {
        int clip = random.Next(1, 3);
        mascotteVoice = (AudioClip) Resources.Load("MascotteSpeech/Error/" + clip);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tryAgain2()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/PhraseDePremiereErreur1");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tryAgain3(Trash dechet)
    {
        mascotteVoice = dechet.rappel;
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public void retry(Trash dechet)
    {

    }

    public static void fail(Trash dechet)
    {
        int clip = random.Next(1, 2);
        switch (clip)
        {
            case 1:
                mascotteVoice = dechet.failure1;
                break;

            case 2:
                mascotteVoice = dechet.failure2;
                break;

        }
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void presentation()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/CaracterPresentation");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void aideMenu(int index)
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/MainMenuInfos/"+index);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void aideLancerPartie()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/ProfileScreenConnexionFromExistingProfile");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void aideChoixNiveau()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/DifficultyChoiceScreen");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    private static void aideMenuNiveau()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/aideMenuNiveau");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tutorialConnexion(String animal)
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Tutorial/Connexion" + animal);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tutorialDifficultyLevel()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Tutorial/MenuDifficulte");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tutorialMainMenu()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Tutorial/MenuPrincipal");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tutorialInfoPoubelle( String poubelle)
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Tutorial/"+poubelle);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void tutorialPoubelle(String couleur)
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Tutorial/Poubelle"+couleur);
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void endTutorialSpeech()
    {
        mascotteVoice = (AudioClip)Resources.Load("MascotteSpeech/Tutorial/End");
        voice.clip = mascotteVoice;
        voice.Play();
    }

    public static void stopAudio()
    {
        if (voice.isPlaying)
        {
            voice.Stop();
        }
    }

    public static AudioSource getVoice()
    {
        return voice;
    }
}
