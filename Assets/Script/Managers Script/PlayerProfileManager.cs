﻿using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class PlayerProfileManager : MonoBehaviour
{
    public static Profile usedProfile;

    public static void updateSoundParameters(float musicVol, float mascotteVol, float effectVol)
    {
        usedProfile.musicVol = musicVol;
        usedProfile.mascotteVol = mascotteVol;
        usedProfile.effectsVol = effectVol;
        Save();
    }

    public static void updateGameStats(int level, bool allCorrect, float time, int nbOfGreenBinTrash, int nbOfRedBinTrash, int nbOfYellowBinTrash, int nbOfGreyBinTrash)
    {
        if (level ==3 && allCorrect && usedProfile.bestTime > time) //on n'améliore le best time que si on était en niveau chronometre (niveau 3) et si toutes les réponses sont justes
        {
            usedProfile.bestTime = time;
        }
        usedProfile.nbOfGamePlayed++;
        usedProfile.nbOfGreenBinTrash += nbOfGreenBinTrash;
        usedProfile.nbOfRedBinTrash += nbOfRedBinTrash;
        usedProfile.nbOfYellowBinTrash += nbOfYellowBinTrash;
        usedProfile.nbOfGreyBinTrash += nbOfGreyBinTrash;
        Save();
    }

    public static void Save()
    {
        string jsonData = JsonUtility.ToJson(usedProfile, true);
        PlayerPrefs.SetString(usedProfile.playerPrefKey, jsonData);
        PlayerPrefs.Save();
    }

    public static void updatePresentationTutorial()
    {
        PlayerProfileManager.usedProfile.presentationTutorial = false;
        Save();
    }

    public static void updateMenuTutorial()
    {
        usedProfile.mainMenuTutorial = false;
        Save();
    }

    public static void updateDifficultyTutorial()
    {
        usedProfile.levelSelectionTutorial = false;
        Save();
    }

    public static void updateGameplayTutorial()
    {
        usedProfile.gameplayTutorial = false;
        Save();
    }
}
