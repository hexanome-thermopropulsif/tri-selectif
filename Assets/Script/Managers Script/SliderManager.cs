﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SliderManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        GameObject gameObjectMusic = GameObject.Find("SliderVolumeMusique");
        GameObject gameObjectMascotte = GameObject.Find("SliderVolumeMascotte");
        GameObject gameObjectEffects = GameObject.Find("SliderVolumeEffets");

        Slider sliderMusic = gameObjectMusic.GetComponent<Slider>();
        Slider sliderMascotte = gameObjectMascotte.GetComponent<Slider>();
        Slider sliderEffects = gameObjectEffects.GetComponent<Slider>();

        sliderMusic.value = VolumeManager.musicVolume;
        sliderMascotte.value = VolumeManager.mascotteVolume;
        sliderEffects.value = VolumeManager.effectsVolume;

        sliderMusic.onValueChanged.AddListener(setMusicVolume);
        sliderMascotte.onValueChanged.AddListener(setMascotteVolume);
        sliderEffects.onValueChanged.AddListener(setEffectsVolume);
    }

    private void setMusicVolume(float value) {
        VolumeManager.setMusicVolume(value);
    }

    private void setMascotteVolume(float value) {
        VolumeManager.setMascotteVolume(value);
    }

    private void setEffectsVolume(float value) {
        VolumeManager.setEffectsVolume(value);
        EffectsSoundsManager.playClicSound();
    }
}
