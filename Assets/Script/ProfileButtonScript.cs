﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ProfileButtonScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        Profile profile = PlayerProfileManager.usedProfile;

        GameObject animalImage = new GameObject("ProfilePicture", typeof(RectTransform));
        animalImage.transform.SetParent(gameObject.transform);

        RectTransform transform = animalImage.GetComponent<RectTransform>();
        transform.SetParent(gameObject.transform);

        Texture image = Resources.Load<Texture2D>(profile.imagePath);
        animalImage.AddComponent<RawImage>().texture = image;
        transform.anchoredPosition = new Vector2(0,0);
        transform.sizeDelta = new Vector2(100, 100);
        transform.localScale = new Vector3(1f, 1f, 1f);
    }
}
