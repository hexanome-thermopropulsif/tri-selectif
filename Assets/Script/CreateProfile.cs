﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;
using UnityEngine.EventSystems;
using UnityEngine.UI;
using UnityEngine.SceneManagement;
using System;

public class CreateProfile : MonoBehaviour
{
    private int nbProfiles;
    private Profile newProfile;

    public GameObject errorPanelUI;

    private void Start()
    {
        errorPanelUI.SetActive(false);
    }

    public void SaveProfile()
    {
        nbProfiles = 1;

        string playerPrefKey = "profile" + nbProfiles;

        while (PlayerPrefs.HasKey(playerPrefKey))
        {
            nbProfiles++;
            playerPrefKey = "profile" + nbProfiles;
        }

        if(nbProfiles > 6)
        {
            //limite de profils créés atteinte
            errorPanelUI.SetActive(true);
            return;
        }
        else
        {
            newProfile = ScriptableObject.CreateInstance<Profile>();

            string nomHero = GameObject.Find("NomHero").GetComponent<TextMeshProUGUI>().text;

            if (String.IsNullOrEmpty(nomHero))
            {
                nomHero = "lion";   //default Icon
            }

            newProfile.imagePath = "Icones/" + nomHero;

            string newProfileName = GameObject.Find("InputNom").GetComponent<TMP_InputField>().text;
            if (String.IsNullOrEmpty(newProfileName))
            {
                newProfileName = playerPrefKey;
            }
            newProfile.profileName = newProfileName;
            newProfile.playerPrefKey = playerPrefKey;

            //Init best time and volume Pref
            newProfile.bestTime = Controller.TIME_TIMIT;
            newProfile.musicVol = 0.1f;
            newProfile.mascotteVol = 0.5f;
            newProfile.effectsVol = 0.25f;

            newProfile.levelSelectionTutorial = true;
            newProfile.gameplayTutorial = true;
            newProfile.presentationTutorial = true;
            newProfile.mainMenuTutorial = true;

            string jsonData = JsonUtility.ToJson(newProfile, true);
            PlayerPrefs.SetString(newProfile.playerPrefKey, jsonData);
            PlayerPrefs.Save();

            //ensuite le menu est chargé 
            PlayerProfileManager.usedProfile = newProfile; //on garde en memoire sur quel profil on est
            SceneManager.LoadScene("ChooseProfileScene");
        }
    }

    public void UpdateHeroName()
    {
        string name = EventSystem.current.currentSelectedGameObject.GetComponentInChildren<RawImage>().texture.name;
        
        GameObject.Find("NomHero").GetComponent<TextMeshProUGUI>().text = name;
    }
}
