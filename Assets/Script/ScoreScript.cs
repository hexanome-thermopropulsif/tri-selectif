﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class ScoreScript : MonoBehaviour
{
    private ScoreManager scoreManager;

    private GameObject poubelleVerte;
    private GameObject poubelleRouge;
    private GameObject poubelleJaune;
    private GameObject poubelleBleue;

    private RawImage imageVerte;
    private RawImage imageRouge;
    private RawImage imageJaune;
    private RawImage imageBleue;

    private TextMeshProUGUI texteVerte;
    private TextMeshProUGUI texteRouge;
    private TextMeshProUGUI texteJaune;
    private TextMeshProUGUI texteBleue;

    public Texture2D mascotteContente;
    public Texture2D mascotteNormale;
    public Texture2D mascotteTriste;


    // Start is called before the first frame update
    void Start()
    {
        scoreManager = ScoreManager.Instance;

        poubelleVerte = GameObject.Find("Score Verte");
        poubelleRouge = GameObject.Find("Score Rouge");
        poubelleJaune = GameObject.Find("Score Jaune");
        poubelleBleue = GameObject.Find("Score Bleue");

        imageVerte = poubelleVerte.GetComponentInChildren<RawImage>();
        imageRouge = poubelleRouge.GetComponentInChildren<RawImage>();
        imageJaune = poubelleJaune.GetComponentInChildren<RawImage>();
        imageBleue = poubelleBleue.GetComponentInChildren<RawImage>();

        texteVerte = poubelleVerte.GetComponentInChildren<TextMeshProUGUI>();
        texteRouge = poubelleRouge.GetComponentInChildren<TextMeshProUGUI>();
        texteJaune = poubelleJaune.GetComponentInChildren<TextMeshProUGUI>();
        texteBleue = poubelleBleue.GetComponentInChildren<TextMeshProUGUI>();

        imageVerte.color = new Color(0.25f, 1f, 0f);
        imageRouge.color = new Color(1f, 0.25f, 0f);
        imageJaune.color = new Color(1f, 1f, 0.25f);
        imageBleue.color = new Color(0f, 0f, 1f);

        int nombreVerte = 0;
        int nombreRouge = 0;
        int nombreJaune = 0;
        int nombreBleue = 0;

        int nombreVerteCorrect = 0;
        int nombreRougeCorrect = 0;
        int nombreJauneCorrect = 0;
        int nombreBleueCorrect = 0;

        for (int i = 0; i < Controller.SERIES_COUNT; i++)
        {
            if (scoreManager.getBinName(i) == "TrashbinGreen") {
                nombreVerte++;

                if (scoreManager.getResult(i) == 1) {
                    nombreVerteCorrect++;
                }
            }

            if (scoreManager.getBinName(i) == "TrashbinRed") {
                nombreRouge++;

                if (scoreManager.getResult(i) == 1) {
                    nombreRougeCorrect++;
                }
            }

            if (scoreManager.getBinName(i) == "TrashbinYellow") {
                nombreJaune++;

                if (scoreManager.getResult(i) == 1) {
                    nombreJauneCorrect++;
                }
            }

            if (scoreManager.getBinName(i) == "TrashbinBlue") {
                nombreBleue++;

                if (scoreManager.getResult(i) == 1) {
                    nombreBleueCorrect++;
                }
            }
        }

        texteVerte.SetText("{0}/{1}", nombreVerteCorrect, nombreVerte);
        texteRouge.SetText("{0}/{1}", nombreRougeCorrect, nombreRouge);
        texteJaune.SetText("{0}/{1}", nombreJauneCorrect, nombreJaune);
        texteBleue.SetText("{0}/{1}", nombreBleueCorrect, nombreBleue);

        RawImage mascotteImage = GameObject.Find("MascotteImage").GetComponent<RawImage>();
        mascotteImage.texture = GetMascotteOnScore();

        TextMeshProUGUI titreText = GameObject.Find("FelicitationsText").GetComponent<TextMeshProUGUI>();
        titreText.text = GetTitreOnScore();

        PlayerProfileManager.updateGameStats(Controller.GetNiveau(), (scoreManager.getScore() - Controller.SERIES_COUNT) == 0, scoreManager.time, nombreVerteCorrect, nombreRougeCorrect, nombreJauneCorrect, nombreBleueCorrect); ;
    }

    public Texture2D GetMascotteOnScore() {
        int endScore = scoreManager.getScore();

        if (endScore >= 9) {
            return mascotteContente;   
        } else if (endScore > 5) {
            return mascotteNormale;
        } else {
            return mascotteTriste;
        }
    }

    public string GetTitreOnScore() {
        int endScore = scoreManager.getScore();

        if (endScore >= 9) {
            return "Bravo ! Super !";   
        } else if (endScore > 5) {
            return "Bien joué";
        } else {
            return "Dommage !";
        }
    }
}
