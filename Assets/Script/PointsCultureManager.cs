﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PointsCultureManager : MonoBehaviour
{
    public PointCulture[] pointCultures;
    
    private static PointsCultureManager instance = null;
    public static PointsCultureManager Instance
    {
        get { return instance; }
    }

    private void Awake()
    {
        if(instance != null && instance != this)
        {
            Destroy(this.gameObject);
            return;
        }
        else
        {
            instance = this;
        }
    }

    public static PointCulture GetRandomPointCulture()
    {
        var rand = new System.Random();
        int index = rand.Next(7);

        return instance.pointCultures[index];
    }

    public static PointCulture[] GetAllPointsCulture() {
        return instance.pointCultures;
    }

/*
    private static string pointCulture_BouteillePlastique() {
        return "Le plastique d'une bouteille peut servir à fabriquer une autre bouteille en plastique et avec 6 bouteilles d'eau recyclées, on fabrique un ours en peluche.";
    }

    private static string pointCulture_BouteilleVerre() {
        return "Avec une bouteille en verre, on fabrique une autre bouteille en verre et ainsi de suite car le verre se recycle à l'infini…";
    }

    private static string pointCulture_Cannette() {
        return "Avec 570 canettes d'aluminium recyclées, on fabrique une chaise.";
    }

    private static string pointCulture_BoiteCarton() {
        return "Avec 4 boîtes en carton de céréales recyclées, on fabrique 1 boîte à chaussures.";
    }

    private static string pointCulture_Papier() {
        return "Tous les papiers se recyclent très bien. En recyclant du papier, tu permets d'économiser beaucoup d'eau et d'énergie !";
    }

    private static string pointCulture_FlaconPlastique() {
        return "Avec 7 flacons de lessive en plastique, on fabrique 1 siège voiture pour bébé.";
    }

    private static string pointCulture_Conserves() {
        return "Avec 9 boîtes de conserve recyclées, on fabrique 1 boule de pétanque… Et avec 7849 boîtes de conserves recyclées, on fabrique un chariot de supermarché.";
    }
    */
}
