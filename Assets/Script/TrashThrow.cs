﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TrashThrow : MonoBehaviour
{

    Vector3 direction;
    Transform targetBin;
    public Controller controller;

    [SerializeField]
    private float speed = 1f;

    private Vector3 binHeight = new Vector3(0,2.5f,0);

    // Start is called before the first frame update
    void Start()
    {
        targetBin = null;
        direction = new Vector3(0, 0, 0);
    }

    // Update is called once per frame
    void Update()
    {
        if (targetBin==null && Input.GetMouseButtonDown(0)) //only get the first click
        {
            var ray = Camera.main.ScreenPointToRay(Input.mousePosition);

            RaycastHit hitInfos;
            if (Physics.Raycast(ray, out hitInfos) && hitInfos.transform.gameObject.tag == "Bin")
            {
                targetBin = hitInfos.transform;

                targetBin.GetComponentInParent<Animator>().SetBool("open", true);
                direction = targetBin.position + binHeight - transform.position;
            }
        }

        if (targetBin != null &&
            Vector2.Distance(new Vector2(transform.position.x, transform.position.z),
                            new Vector2(targetBin.position.x, transform.position.z))
            < 0.02 && direction.y == 0)
        {
            transform.position = targetBin.position + binHeight;
            direction = new Vector3(0, -4, 0);
        } else if (transform.position.z > 5)
        {
            transform.position = targetBin.position + binHeight;
            direction = new Vector3(0, -4, 0);
        }

        transform.position += direction * speed * Time.deltaTime;

        if (GetComponent<Transform>().position.y < 1.5)
        {
            targetBin.GetComponentInParent<Animator>().SetBool("open", false);
            if (GetComponent<Transform>().position.y < 0.5)
            {
                Destroy(gameObject);
                controller.lancer(targetBin.gameObject);
            }
        }
    }
}
