﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class PauseMenu : MonoBehaviour
{
	public GameObject pauseMenuUI;

	public static bool gamePaused = false;

	private GameObject pauseButton;

	private void Start()
	{
		pauseButton = GameObject.Find("PauseButton");
		gamePaused = false;
		pauseButton.SetActive(true);
	}

	public void onPauseClick()
	{
		if (gamePaused)
		{
			Resume();
		}
		else
		{
			Pause();
		}
	}

	public void Resume()
	{
		EffectsSoundsManager.playClicSound();
		pauseMenuUI.SetActive(false);
		Time.timeScale = 1f;
		GameObject[] bins = GameObject.FindGameObjectsWithTag("Bin");
		foreach (var bin in bins)
		{
			bin.layer = 0;	//default layer
		}
		gamePaused = false;
		pauseButton.SetActive(true);
	}

	void Pause()
	{
		EffectsSoundsManager.playClicSound();
		pauseMenuUI.SetActive(true);
		Time.timeScale = 0f;
		GameObject[] bins = GameObject.FindGameObjectsWithTag("Bin");
		foreach(var bin in bins)
		{
			bin.layer = 2;	//ignore raycast
		}
		gamePaused = true;
		pauseButton.SetActive(false);
	}

	public void LoadMenu()
	{
		EffectsSoundsManager.playClicSound();
		Time.timeScale = 1f;
		SceneManager.LoadScene("LoadingScene");
		LoadingSceneScript.nextScene = "MenuScene";
	}

	public void QuitGame()
	{
		EffectsSoundsManager.playClicSound();
		Application.Quit();
	}
}
