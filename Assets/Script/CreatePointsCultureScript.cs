﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;

public class CreatePointsCultureScript : MonoBehaviour
{
    PointCulture[] pointsCulture;
    int height = 300/7;
    GameObject[] pointCultureGOs = new GameObject[7];

    public GameObject prefabPointCulture;

    // Start is called before the first frame update
    void Start()
    {
        pointsCulture = PointsCultureManager.GetAllPointsCulture();

        for (int i = 0; i < 7; i++)
        {
            createPointCultureGameObject(i);
        }
    }

    private void createPointCultureGameObject(int index)
    {
        GameObject pointCultureGO = Instantiate(prefabPointCulture) as GameObject;
        pointCultureGOs[index] = pointCultureGO;

        RectTransform transformGO = pointCultureGO.GetComponent<RectTransform>();
        transformGO.SetParent(gameObject.transform);

        transformGO.offsetMax = new Vector2(0f, -height*index); //Right and (PosY+Height)
        transformGO.offsetMin = new Vector2(0f, -height*(index+1)); //Left and PosY

        transformGO.localScale = new Vector3(1f, 1f, 1f);

        TextMeshProUGUI pointCultureText = pointCultureGO.GetComponentInChildren<TextMeshProUGUI>();
        //pointCultureText.fontSize = height/2;
        pointCultureText.text = pointsCulture[index].texte_point_culture;
        //pointCultureText.color = new Color(0f, 0f, 0f);

        Button pointCultureButton = pointCultureGO.GetComponentInChildren<Button>();
        pointCultureButton.onClick.AddListener(() => onButtonClickMethod(index, pointCultureText));
    }

    public void onButtonClickMethod(int index, TextMeshProUGUI textMesh) {
        MascotteManager.playClip(pointsCulture[index].audio_point_culture);
        textMesh.color = new Color(0f, 1f, 0f);
    }
}
