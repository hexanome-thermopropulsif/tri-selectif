﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using TMPro;

public class LoadingSceneScript : MonoBehaviour
{
    GameObject loadingBar;
    GameObject textInfo;
    float width;
    public static string nextScene;

    // Start is called before the first frame update
    void Start()
    {
      textInfo = GameObject.Find("Info");
      loadingBar = GameObject.Find("Fill");
      width = GameObject.Find("LoadingArea").GetComponent<RectTransform>().rect.width;

      textInfo.GetComponent<TextMeshProUGUI>().SetText(PointsCultureManager.GetRandomPointCulture().texte_point_culture);
      StartCoroutine(LoadScene());
    }

    IEnumerator LoadScene()
    {
        yield return null;

        //Begin to load the Scene you specify
        AsyncOperation asyncOperation = SceneManager.LoadSceneAsync(nextScene);
        //Don't let the Scene activate until you allow it to
        asyncOperation.allowSceneActivation = false;
        //When the load is still in progress, output the Text and progress bar
        while (!asyncOperation.isDone)
        {
            SetProgression(asyncOperation.progress);

            // Check if the load has finished
            if (asyncOperation.progress >= 0.9f)
            {
				yield return new WaitForSeconds(1);
				SetProgression(0.95f);

				yield return new WaitForSeconds(1);
				SetProgression(0.975f);

				yield return new WaitForSeconds(1);
				SetProgression(0.99f);

				yield return new WaitForSeconds(1);
				SetProgression(1f);
				yield return new WaitForSeconds(1);

				asyncOperation.allowSceneActivation = true;
                break;
            }

            yield return null;
        }
    }

    private void SetProgression(float progress) {
        loadingBar.GetComponent<RectTransform>().sizeDelta = new Vector2(width * progress, 0);
    }
}
