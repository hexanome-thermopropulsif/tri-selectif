﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using TMPro;
using System;

public class ProfilScript : MonoBehaviour
{
    Profile profilActuel = PlayerProfileManager.usedProfile;

    public GameObject buttonCanvas;
    public GameObject baseButton;

    // Start is called before the first frame update
    void Start()
    {
        GameObject partiesJouees = GameObject.Find("GamesPlayed");

        GameObject poubelleVerte = GameObject.Find("Score Verte");
        GameObject poubelleRouge = GameObject.Find("Score Rouge");
        GameObject poubelleJaune = GameObject.Find("Score Jaune");
        GameObject poubelleBleue = GameObject.Find("Score Bleue");
        GameObject bestTimeObj = GameObject.Find("BestTime");


        RawImage imageVerte = poubelleVerte.GetComponentInChildren<RawImage>();
        RawImage imageRouge = poubelleRouge.GetComponentInChildren<RawImage>();
        RawImage imageJaune = poubelleJaune.GetComponentInChildren<RawImage>();
        RawImage imageBleue = poubelleBleue.GetComponentInChildren<RawImage>();


        TextMeshProUGUI texteGames = partiesJouees.GetComponentInChildren<TextMeshProUGUI>();

        TextMeshProUGUI texteVerte = poubelleVerte.GetComponentInChildren<TextMeshProUGUI>();
        TextMeshProUGUI texteRouge = poubelleRouge.GetComponentInChildren<TextMeshProUGUI>();
        TextMeshProUGUI texteJaune = poubelleJaune.GetComponentInChildren<TextMeshProUGUI>();
        TextMeshProUGUI texteBleue = poubelleBleue.GetComponentInChildren<TextMeshProUGUI>();
        TextMeshProUGUI bestTimeTxt = bestTimeObj.GetComponentInChildren<TextMeshProUGUI>();


        imageVerte.color = new Color(0.25f, 1f, 0f);
        imageRouge.color = new Color(1f, 0.25f, 0f);
        imageJaune.color = new Color(1f, 1f, 0.25f);
        imageBleue.color = new Color(0.3f, 0.3f, 0.3f);


        texteGames.SetText("Nombre de parties : {0}", profilActuel.nbOfGamePlayed);
        
        texteVerte.SetText("{0}", profilActuel.nbOfGreenBinTrash);
        texteRouge.SetText("{0}", profilActuel.nbOfRedBinTrash);
        texteJaune.SetText("{0}", profilActuel.nbOfYellowBinTrash);
        texteBleue.SetText("{0}", profilActuel.nbOfGreyBinTrash);

        bestTimeTxt.SetText(String.Format(" {0:0.00}", profilActuel.bestTime)+" secondes");

        spawnButton(profilActuel);
    }

    private GameObject spawnButton(Profile profile)
    {
        GameObject button = Instantiate(baseButton) as GameObject;
        
        button.GetComponent<RectTransform>().SetParent(buttonCanvas.GetComponent<RectTransform>(), false);

        RectTransform parent = buttonCanvas.GetComponent<RectTransform>();

        button.GetComponentInChildren<TextMeshProUGUI>().SetText(profilActuel.profileName);

        Texture image = Resources.Load<Texture2D>(profilActuel.imagePath);
        button.GetComponentInChildren<RawImage>().texture = image;

        button.GetComponent<Button>().interactable = false;

        return button;
    }

    public void DeleteProfile() {
        PlayerPrefs.DeleteKey(profilActuel.playerPrefKey);
        PlayerProfileManager.usedProfile = null;
    }
}
