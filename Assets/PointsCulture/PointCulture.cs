﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "PointCulture", menuName ="Point Culture")]
public class PointCulture : ScriptableObject
{
    public string texte_point_culture;
    public AudioClip audio_point_culture;
    public Texture2D image_point_culture;
}