﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Trash", menuName ="Trash Object")]
public class Trash : ScriptableObject
{
    public string nom;

    public string description;

    public Materiaux material;

    public GameObject model;

    public Vector3 relativePosition;

    public Vector3 rotation;

    public Vector3 localScale;

    public AudioClip presentationClip;
    public AudioClip failure1;
    public AudioClip failure2;
    public AudioClip rappel;

    // override object.Equals
    public override bool Equals(object obj)
    {        
        if (obj == null || GetType() != obj.GetType())
        {
            return false;
        }
        Trash tObj = (Trash)obj;
        return (nom.Equals(tObj.nom) && description.Equals(tObj.description));
    }
    
    // override object.GetHashCode
    public override int GetHashCode()
    {
        return base.GetHashCode();
    }
}
