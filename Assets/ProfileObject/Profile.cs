﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Profile : ScriptableObject
{
    public string playerPrefKey;
    public string profileName;

    public string imagePath;

    public int nbOfGamePlayed;

    public int nbOfGreenBinTrash;
    public int nbOfRedBinTrash;
    public int nbOfYellowBinTrash;
    public int nbOfGreyBinTrash;

    public float musicVol;
    public float mascotteVol;
    public float effectsVol;

    public float bestTime;

    public bool presentationTutorial;
    public bool mainMenuTutorial;
    public bool levelSelectionTutorial;
    public bool gameplayTutorial;

}
